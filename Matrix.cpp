//
// Created by Иван Кушнаренко on 30.03.2019.
//

#include "Matrix.h"
#include <iostream>
#include <iomanip>
#include <utility>
#include <cmath>
#include <limits>
#define EPS std::numeric_limits<double>::epsilon()

using namespace std;


Matrix::Matrix():rows_(1), columns_(1)
{
	data = new double*[rows_]{};
	for (size_t i =0; i < rows_; i++)
		data[i] = new double[columns_]{};
}

Matrix::Matrix(int M, int N): rows_(M), columns_(N)
{
    if((rows_ <= 0) || (columns_ <= 0))
        throw domain_error("Wrong size of matrix when initializing ");
    data = new double*[rows_]{};
    for (size_t i = 0; i < rows_; i++)
        data[i] = new double[columns_]{};
}

Matrix::Matrix(const Matrix &other): rows_(other.rows()), columns_(other.columns())
{
	data = new double*[rows_];
	for (size_t i = 0; i < rows_; i++)
	{
		data[i] = new double[columns_];
		for (size_t j = 0; j < columns_; j++)
			data[i][j] = other.data[i][j];
	}
}

Matrix::Matrix(Matrix &&other) noexcept: rows_(other.rows_), columns_(other.columns_)
{
	data = other.data;
	other.data = nullptr;
}

Matrix::~Matrix()
{
    clear();
}

Matrix& Matrix::operator=(const Matrix &other)
{
	if (*this != other)
	{
		clear();
		rows_ = other.rows();
		columns_ = other.columns();
		data = new double *[rows_] {};
		for (size_t i = 0; i < rows_; i++)
		{
			data[i] = new double[columns_] {};
			for (size_t j = 0; j < columns_; j++)
				data[i][j] = other.data[i][j];
		}
	}
    return *this;
}

Matrix& Matrix::operator=(Matrix&& other) noexcept
{
	clear();
	rows_ = other.rows();
	columns_ = other.columns();
	data = other.data;
	other.data = nullptr;
	return *this;
}

Matrix& Matrix::operator=(initializer_list<double> il)
{
	auto iter = il.begin();
	for (auto &element : *this)
	{
		if (iter != il.end())
		{
			element = *iter;
			++iter;
		}
		else
			element = 0;
	}
	return *this;
}

size_t Matrix::rows() const noexcept
{
    return rows_;
}
size_t Matrix::columns() const noexcept
{
    return columns_;
}

Matrix::operator std::string() const
{
    string result;
    for (size_t i = 0; i < rows(); i++)
    {
    	result += '(';
        for (size_t j = 0; j < columns(); j++)
        {
            if (j != columns()-1)
                result += tostring(data[i][j]) + "  ";
            else
                result += tostring(data[i][j]);
        }
		result += ')';
        if (i != rows()-1)
            result += '\n';
    }
    return result;
}

Matrix Matrix::operator+(const Matrix &other) const
{
	if (this->rows() != other.rows() || this->columns() != other.columns())
		throw invalid_argument("Adding matrix with different sizes");
	Matrix res(*this);
	for (size_t i = 0; i < res.rows(); ++i)
		for (size_t j = 0; j < res.columns(); ++j)
			res.data[i][j] += other.data[i][j];
	return res;
}

Matrix Matrix::operator-(const Matrix &other) const
{
	if (this->rows() != other.rows() || this->columns() != other.columns())
		throw invalid_argument("Subtraction matrix with different size");
	Matrix res(*this);
	for (size_t i = 0; i < res.rows(); ++i)
		for (size_t j = 0; j < res.columns(); ++j)
			res.data[i][j] -= other.data[i][j];
	return res;
}

Matrix Matrix::operator*(const Matrix &other) const
{
    if (this->columns() != other.rows())
        throw invalid_argument("Width of the first matrix should be equal to the height of the second one when multiplying");
    Matrix result(this->rows(), other.columns());
    for (size_t i = 0; i < result.rows(); i++)
    {
        for (size_t j = 0; j < result.columns(); j++)
        {
            for (size_t k = 0; k < this->columns(); k++)
				result.data[i][j] += this->data[i][k]*other.data[k][j];
        }
    }
    return result;
}

Matrix Matrix::operator*(double x) const
{
	Matrix res(*this);
	for (size_t i = 0; i < rows(); ++i)
	{
		for (size_t j = 0; j < columns(); ++j)
			res.data[i][j] *= x;
	}
	return res;
}

Matrix Matrix::operator~() const
{
	Matrix res(columns(), rows());
	for (size_t i = 0; i < res.rows(); ++i)
	{
		for (size_t j = 0; j < res.columns(); ++j)
			res.data[i][j] = this->data[j][i];
	}
	return res;
}

bool Matrix::operator==(const Matrix &other) const noexcept
{
	if (!(this->rows() == other.rows() && this->columns() == other.columns()))
		return false;
	for (size_t i = 0; i < rows(); i++)
		for (size_t j = 0; j < columns(); j++)
			if (this->data[i][j] !=  other.data[i][j])
				return false;
	return true;
}

bool Matrix::operator!=(const Matrix &other) const noexcept
{
	return !(*this == other);
}

const double* Matrix::operator[](size_t i) const noexcept
{
	return data[i];
}
double* Matrix::operator[](size_t i) noexcept
{
	return data[i];
}

double Matrix::at(size_t row, size_t column) const
{
	if (row < 0 || row >= rows() || column < 0 || column >= columns())
		throw out_of_range("Wrong indexes for matrix");
	return data[row][column];
}

double& Matrix::at(size_t row, size_t column)
{
	if (row < 0 || row >= rows() || column < 0 || column >= columns())
		throw out_of_range("Wrong indexes for matrix");
	return data[row][column];
}

double Matrix::det() const
{
    if(rows() != columns())
    	throw invalid_argument("Trying to calculate determinant of non-square matrix");
	Matrix tmp = *this;
    double result = 1;
    for (size_t j = 0; j < tmp.columns(); j++)
    {
        size_t lead = tmp.findLeadElementInColumn_(j, j);
        if (lead == -1) // (-1) - state for not found lead element
            return 0; // there is no lead element in column <=> all elements in this column are 0
        if (lead != j)
        	tmp.swapRows_(lead, j);
		tmp.setZeroBelow_(j, j);
        result *= tmp.data[j][j];
    }
    return tmp.detSign * result;
}

Matrix& Matrix::fillRandom(int radius) noexcept
{
	const int max_value = RAND_MAX / 10;
	radius = abs(radius);
	if (radius > max_value)
		radius = max_value;
	srand(time(nullptr));
	for (auto &elem: *this)
		elem = int((rand() % (radius * 10) - radius/2) / 10.);
	return *this;
}

size_t Matrix::findLeadElementInRow_(size_t row, size_t startPos, size_t endPos) const noexcept
{
	size_t currentColumn = max(startPos, size_t(0));
	size_t end = min(columns(), endPos);
	while ((currentColumn < end) && (is_equal(data[row][currentColumn], 0)))
			++currentColumn;
	if (currentColumn >= end)
		return -1;
	else
		return currentColumn;
}

size_t Matrix::findLeadElementInColumn_(size_t column, size_t startPos, size_t endPos) const noexcept
{
    size_t currentRow = max(startPos, size_t(0));
    size_t end = min(rows(), endPos);
    while (currentRow < end && (is_equal(data[currentRow][column], 0)))
    	++currentRow;
    if (currentRow >= end)
        return -1;
    return currentRow;
}

void Matrix::swapRows_(size_t i, size_t j) noexcept
{
    for (size_t k = 0; k < columns(); k++)
        swap(data[i][k], data[j][k]);
    detSign *= -1;
}
void Matrix::swapColumns_(size_t i, size_t j) noexcept
{
	for (size_t k = 0; k < rows(); k++)
		swap(data[k][i], data[k][j]);
	detSign *= -1;
}

void Matrix::setZeroBelow_(size_t leadRow, size_t leadColumn) noexcept
{
	double coef = 0;
    for (size_t i = leadRow + 1; i < rows(); i++)
    {
        coef = data[i][leadColumn]/data[leadRow][leadColumn];
        for (size_t j = 0; j < columns(); j++)
		{
        	if (j != leadColumn)
        		data[i][j] -= coef * data[leadRow][j];
        	else
        		data[i][j] = 0;
		}
    }
}

void Matrix::clear() noexcept
{
	if (data)
	{
		for (size_t i = 0; i < rows_; ++i)
		{
			delete[] data[i];
			data[i] = nullptr;
		}
		delete[] data;
		data = nullptr;
	}
}

Matrix::iterator Matrix::begin() const noexcept
{
	return iterator(this);
}
Matrix::iterator Matrix::end() const noexcept
{
	return iterator(this, true);
}

std::ostream& operator<<(std::ostream &out, const Matrix &m) noexcept
{
	out << fixed << setprecision(3) <<  string(m);
	return out;
}

std::istream& operator>>(std::istream &in, Matrix &m)
{
	for (auto &element: m)
	{
		if (in.good())
			in >> element;
		else
			element = 0;
	}
	return in;
}

size_t Matrix::rank() const noexcept
{
	size_t rank = 0;
	size_t leadRow = 0;
	Matrix tmp(*this);
	for (size_t j = 0; j < columns(); ++j)
	{
		leadRow = tmp.findLeadElementInColumn_(j, j);
		if (leadRow != -1)
		{
			if (leadRow != j)
				tmp.swapRows_(leadRow, j);
			tmp.setZeroBelow_(j, j);
			rank++;
		}
	}
	return rank;
}

size_t Matrix::findLeadElementInRow(size_t row, size_t startPos, size_t endPos) const
{
	if (row < 0 || row >= rows())
		throw std::out_of_range("Finding lead element in row which is out of range");
	return findLeadElementInRow_(row, startPos, endPos);
}

size_t Matrix::findLeadElementInColumn(size_t column, size_t startPos, size_t endPos) const
{
	if (column < 0 || column >= columns())
		throw std::out_of_range("Finding lead element in column which is out of range");
	return findLeadElementInColumn_(column, startPos, endPos);
}

void Matrix::swapRows(size_t i, size_t j)
{
	if (i < 0 || j < 0 || i >= rows() || j >= rows())
		throw std::out_of_range("swapRows(): One of parameter is out of range");
	return swapRows_(i, j);
}

void Matrix::swapColumns(size_t i, size_t j)
{
	if (i < 0 || j < 0 || i >= columns() || j >= columns())
		throw std::out_of_range("swapColumns(): One of parameter is out of range");
	return swapColumns_(i, j);
}

void Matrix::setZeroBelow(size_t leadRow, size_t leadColumn)
{
	if (leadRow < 0 || leadColumn < 0 || leadRow >= rows() || leadColumn >= columns())
		throw std::out_of_range("serZeroBelow(): One of parameter is out of range");
	return setZeroBelow_(leadRow, leadColumn);
}

bool Matrix::is_equal(double x, double y) noexcept
{
	return fabs(x-y) < EPS;
}


Matrix::iterator::iterator(const Matrix *m, bool end_): matrix(const_cast<Matrix *>(m)), end(end_)
{
	if (!end)
	{
		cur_row = 0;
		cur_col = 0;
	}
	else
	{
		cur_row = matrix->rows() - 1;
		cur_col = matrix->columns() - 1;
	}
}

double Matrix::iterator::operator*() const
{
	if (cur_row < 0 || cur_row >= matrix->rows() || cur_col < 0 || cur_col >= matrix->columns())
		throw out_of_range("Wrong indexes for matrix when indirection");
	return (*matrix)[cur_row][cur_col];
}
double& Matrix::iterator::operator*()
{
	if (cur_row < 0 || cur_row >= matrix->rows() || cur_col < 0 || cur_col >= matrix->columns())
		throw out_of_range("Wrong indexes for matrix when indirection");
	return const_cast<double &>((*matrix)[cur_row][cur_col]);
}


Matrix::iterator& Matrix::iterator::operator++()
{
	if (cur_row < 0 || cur_col < 0)
		throw out_of_range("Iterator position is negative");
	if (cur_col == matrix->columns() - 1)
	{
		if (cur_row != matrix->rows() - 1)
		{
			cur_row += 1;
			cur_col = 0;
		}
		else
			end = true;
	}
	else
		cur_col += 1;
	return *this;
}

bool Matrix::iterator::operator==(const Matrix::iterator &other) const noexcept
{
	return (this->end == other.end && this->cur_row == other.cur_row &&
			this->cur_col == other.cur_col && this->matrix == other.matrix);
}
bool Matrix::iterator::operator!=(const Matrix::iterator &other) const noexcept
{
	return !(*this == other);
}

string tostring(double x)
{
	string s = to_string(x);
	auto dot = s.find('.');
	auto i = s.find_last_not_of('0');
	if (i > dot)
		s.erase(i+1);
	else
		s.erase(dot);
	return s;
}

double det(const Matrix& A)
{
	return A.det();
}

size_t rank(const Matrix &A) noexcept
{
	return A.rank();
}
