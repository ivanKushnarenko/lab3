//
// Created by Иван Кушнаренко on 30.03.2019.
//

#ifndef DETERMINANT_MATRIX_H
#define DETERMINANT_MATRIX_H


#include <string>
#include <initializer_list>
#include <iostream>
#include <vector>


class Matrix
{
	friend std::ostream& operator<<(std::ostream &out, const Matrix &matrix) noexcept;
	friend std::istream& operator>>(std::istream &in, Matrix &matrix);
public:
	class iterator;
	[[nodiscard]] iterator begin() const noexcept;
	[[nodiscard]] iterator end() const noexcept;
public:
	Matrix();
    Matrix(int M, int N);
    // creates M x N matrix, initialize with 0
    // throw invalid_argument if M or N <= 0
    Matrix(const Matrix &other);
    Matrix(Matrix &&other) noexcept;
    ~Matrix();
    Matrix& operator=(const Matrix& other);
    Matrix& operator=(Matrix &&other) noexcept;
    Matrix& operator=(std::initializer_list<double> il);
	/**
	 * Construct matrix from initialization list, writing matrix row by row.
	 * If 'il' size < matrix size, rest of matrix element will be filled by 0
	 * If 'il' size > matrix size, rest of 'il' elements won't be wrote
	 */

    Matrix operator+(const Matrix &other) const;
    Matrix operator-(const Matrix &other) const;
	Matrix operator*(const Matrix &other) const;
	Matrix operator*(double x) const;
	Matrix operator~() const ;

	bool operator==(const Matrix &other) const noexcept;
	bool operator!=(const Matrix &other) const noexcept;

	const double* operator[](size_t i) const noexcept;
	double* operator[](size_t i) noexcept;
	[[nodiscard]] double at(size_t row, size_t column) const;
	double& at(size_t row, size_t column);

    [[nodiscard]] size_t rows() const noexcept; // returns number of rows
    [[nodiscard]] size_t columns() const noexcept; // returns number of columns

    [[nodiscard]] double det() const;
	[[nodiscard]] size_t rank() const noexcept;
    Matrix& fillRandom(int radius=10) noexcept;
    /**
     * Fill matrix with random double (precision 1) numbers from (-|radius|; |radius|).
     * Radius cannot be bigger than floor(RAND_MAX / 10), else it manually sets to this value
     */

    [[nodiscard]] size_t findLeadElementInRow(size_t row, size_t startPos= 0, size_t endPos= -1) const;
	/**
	 * Returns index of first (with the least index) lead element in the row.
	 * Lead element is being searched from 'starPos'.
	 * It checks till 'endPos' or the end of the row is reached.
	 * Returns -1 if there is no lead element in the row
	 * 	(every element in this row from 'startPos' to 'endPos' is 0).
	 * Lead element - here: any non-zero element in the row.
	 * Throws out_of_range if 'row' is inappropriate.
	 */
	[[nodiscard]] size_t findLeadElementInColumn(size_t column, size_t startPos = 0, size_t endPos= -1) const;
	/**
	 * Returns index of first (with the least index) lead element in the column.
	 * Lead element is being searched from 'startPos'.
	 * It checks till 'endPos' or the end of the column is reached.
	 * Returns -1 if there is no lead element in this column
	 * 	(every element in this column from 'startPos' to 'endPos' is 0).
	 * Lead element - here: any non-zero element at the column.
	 * Throws out_of_range if 'column' is inappropriate.
	 */
	void swapRows(size_t i, size_t j);
	/// Throws out_of_range if 'i' or 'j' are inappropriate.
	void swapColumns(size_t i, size_t j);
	/// Throws out_of_range if 'i' or 'j' are inappropriate.
	void setZeroBelow(size_t leadRow, size_t leadColumn);
	/// Sets all elements in column below lead as 0 by elementary transformation of rows of matrix.
	/// Throws out_of_range if 'leadRow' or 'leadColumn' are inappropriate.

	static bool is_equal(double x, double y) noexcept;
private:
    double **data = nullptr;
	size_t rows_;//number of rows
    size_t columns_;//number of columns

    int detSign = 1;

	explicit operator std::string() const;
    void clear() noexcept;

    [[nodiscard]] size_t findLeadElementInRow_(size_t row, size_t startPos = 0, size_t endPos= -1) const noexcept;
    [[nodiscard]] size_t findLeadElementInColumn_(size_t column, size_t startPos=0, size_t endPos=-1) const noexcept;
    void swapRows_(size_t i, size_t j) noexcept;
    void swapColumns_(size_t i, size_t j) noexcept;
    void setZeroBelow_(size_t leadRow, size_t leadColumn) noexcept;

};

class Matrix::iterator
{
public:
	explicit iterator(const Matrix *m, bool end_=false);

	double operator*() const;
	double& operator*();
	iterator& operator++();
	bool operator==(const iterator &other) const noexcept;
	bool operator!=(const iterator &other) const noexcept;

private:
	Matrix *matrix;
	size_t cur_row, cur_col;
	bool end = false;
};

std::string tostring(double x);
double det(const Matrix &A);
size_t rank(const Matrix &A) noexcept;



#endif //DETERMINANT_MATRIX_H
