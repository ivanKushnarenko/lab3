//
// Created by Иван Кушнаренко on 10.11.2019.
//

#include "functions.h"
#include <stdexcept>
#include <string>

slae::Solution slae::Gauss(const Matrix &A, const Matrix &b)
{
	Solution answer;
	Matrix Ab = extend(A, b); // 'Ab' is extended matrix of A and b

	size_t leadRow = 0;
	for (size_t j = 0; j < Ab.columns() - 1; j++)
	{
		leadRow = Ab.findLeadElementInColumn(j, j);
		if (leadRow == -1)
		{
			if (!j)
				return answer.setInfinitely();
		}
		else
		{
			if (leadRow != j)
				Ab.swapRows(j, leadRow);
			Ab.setZeroBelow(j, j);
		}
	}
	return solve(Ab);
}

Matrix slae::extend(const Matrix &A, const Matrix &b)
{
	// next paragraph checks if matrix can be extended
	if (A.rows() != b.rows())
		throw std::invalid_argument("Arguments of function don't make SLAE");
	else if (b.columns() != 1)
		throw std::invalid_argument("b is not vector, but matrix with size " +
									std::to_string(b.rows()) + "x" + std::to_string(b.columns()));

	Matrix res(A.rows(), A.columns() + 1);

	// going column by column
	for (size_t j = 0; j < res.columns() - 1; ++j)
		for (size_t i = 0; i < res.rows(); ++i)
			res[i][j] = A[i][j];

	for (size_t i = 0; i < res.rows(); ++i)
		res[i][res.columns() - 1] = b[i][0];

	return res;
}

slae::Solution slae::solve(Matrix &AbTriangle) noexcept
{
	Solution answer;
	Matrix solution(AbTriangle.columns() - 1, 1);
	for (int i = AbTriangle.rows() - 1; i >= 0; --i)
	{
		size_t leadColumn = AbTriangle.findLeadElementInRow(i, 0, AbTriangle.columns() - 1);
		if (leadColumn == -1)
		{
			if (!Matrix::is_equal(AbTriangle[i][AbTriangle.columns()-1], 0.))
				return answer.setNone();
			else if (i + 1 <= AbTriangle.columns() - 1 )
				return answer.setInfinitely();
		}
		else if (leadColumn != i)
			return answer.setInfinitely();
		else
		{
			solution.at(i, 0) = AbTriangle[i][AbTriangle.columns() - 1] / AbTriangle[i][leadColumn];
			setZeroUpper(AbTriangle, i, leadColumn);
		}
	}
	return answer.setOnly(solution);
}

void slae::setZeroUpper(Matrix &m, size_t leadRow, size_t leadColumn)
{
	if (leadRow < 0 || leadColumn < 0 || leadRow >= m.rows() || leadColumn >= m.columns())
		throw std::out_of_range("setZeroUpper(): One of parameter is out of range");
	double coef = 0;
	for (int i = int(leadRow) - 1; i >= 0; i--)
	{
		coef = m[i][leadColumn] / m[leadRow][leadColumn];
		for (size_t j = 0; j < m.columns(); j++)
		{
			if (j != leadColumn)
				m[i][j] -= coef * m[leadRow][j];
			else
				m[i][j] = 0;
		}
	}
}

bool slae::Solution::is_valid() const noexcept
{
	return only;
}

Matrix slae::Solution::getSolution() const
{
	if (!is_valid())
		throw std::logic_error("Trying to get invalid solution");
	else
		return solution;
}

slae::Solution slae::Solution::setOnly(const Matrix &solution_) noexcept
{
	only = true;
	none = false;
	infinitely = false;
	this->solution = solution_;
	return *this;
}

slae::Solution slae::Solution::setNone() noexcept
{
	only = false;
	none = true;
	infinitely = false;
	return *this;
}

slae::Solution slae::Solution::setInfinitely() noexcept
{
	only = false;
	none = false;
	infinitely = true;
	return *this;
}

bool slae::Solution::operator==(const slae::Solution &other) const noexcept
{
	return (this->only == other.only &&
	   		this->none == other.none &&
	   		this->infinitely == other.infinitely &&
	   		this->solution == other.solution);
}

#ifdef TESTING
int slaeTest::test_extend()
{
	Matrix A1(3, 3), b1(3, 1), res1(3, 4);
	A1 = {1, 2, 3,
		  4, 5, 6,
		  7, 8, 9};
	b1 = {1,
		  2,
		  3};
	res1 = {1, 2, 3, 1,
			4, 5, 6, 2,
			7, 8, 9, 3};
	try
	{
		if (!(res1 == slae::extend(A1, b1)))
			return 1;
	}
	catch (const std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	catch  (...)
	{
		std::cout << "Unpredictable mistake in extend test 1" << std::endl;
	}
	std::cout << "#extend test 1 is Done" << std::endl;

	Matrix A2(4, 4), b2(3, 1), res2(4, 5);
	A2 = {1, 2, 3, 5,
		  4, 5, 6, 6,
		  7, 8, 9, 8,
		  12, 43, 5, 0};
	b2 = {1,
		  2,
		  3};
	res2 = {1, 2, 3, 5, 1,
			4, 5, 6, 6, 2,
			7, 8, 9, 8, 3,
			12, 43, 5, 0, 0};
	try
	{
		if (!(slae::extend(A2, b2) == res2))
			return 1;
	}
	catch (const std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unpredictable mistake in extend test 2" << std::endl;
	}
	std::cout << "#extend test 2 is Done" << std::endl;

	Matrix A3(4, 4), b3(4, 2), res3(4, 6);
	A3 = {1, 2, 3, 5,
		  4, 5, 6, 6,
		  7, 8, 9, 8,
		  12, 43, 5, 0};
	b3 = {1, 3,
		  2, 5,
		  3, 6,
		  8, 8};
	res3 = {1, 2, 3, 5, 1,
			4, 5, 6, 6, 2,
			7, 8, 9, 8, 3,
			12, 43, 5, 0, 8};
	try
	{
		if (!(res3 == slae::extend(A3, b3)))
			return 1;
	}
	catch (const std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
	std::cout << "#extend test 3 is Done" << std::endl;
	return 0;
}

int slaeTest::test_solve()
{
	Matrix Ab_t1(3,4), res1(3, 1);
	Ab_t1 = {1, 2, 3, -3,
		  	 0, 2, 3, 6,
		  	 0, 0, -1, 4};
	res1 = {-9, 9, -4};
	slae::Solution sol1;
	sol1.setOnly(res1);
	if (!(sol1 == slae::solve(Ab_t1)))
		return 1;
	std::cout << "#solve test 1 is done" << std::endl;

	Matrix Ab_t2(4,4), res2(3, 1);
	Ab_t2 = {1, 2, 3, 1,
	  		 0, -3, 5, 2,
	  		 0, 0, 6, 6,
	  		 0, 0, 0, 0};
	res2 = {-4, 1, 1};
	slae::Solution sol2;
	sol2.setOnly(res2);
	if (!(sol2 == slae::solve(Ab_t2)))
		return 1;
	std::cout << "#solve test 2 is done" << std::endl;

	Matrix Ab_t3(3,4);
	Ab_t3 = {1, 2, 3, 2,
			 0, 4, 5, 3,
			 0, 0, 0, 7};
	slae::Solution sol3;
	sol3.setNone();
	if (!(sol3 == slae::solve(Ab_t3)))
		return 1;
	std::cout << "#solve test 3 is done" << std::endl;

	Matrix Ab_t4(3,4);
	Ab_t2 = {1, 2, -2, 13,
			 0, 3, 3, -7,
			 0, 0, 0, 0};
	slae::Solution sol4;
	sol4.setInfinitely();
	if (!(sol4 == slae::solve(Ab_t4)))
		return 1;
	std::cout << "#solve test 4 is done" << std::endl;

	Matrix Ab_t5(3,4);
	Ab_t5 = {0, 2, -2, 13,
			 0, 0, 3, -7,
			 0, 0, 0, 0};
	slae::Solution sol5;
	sol5.setInfinitely();
	if (!(sol5 == slae::solve(Ab_t5)))
		return 1;
	std::cout << "#solve test 5 is done" << std::endl;

	Matrix Ab_t6(2,4);
	Ab_t2 = {1, 2, 3, 5,
			 0, 0, 3, -7};
	slae::Solution sol6;
	sol6.setInfinitely();
	if (!(sol6 == slae::solve(Ab_t6)))
		return 1;
	std::cout << "#solve test 6 is done" << std::endl;
	return 0;
}
#endif