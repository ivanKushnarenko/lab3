//
// Created by Иван Кушнаренко on 08.11.2019.
//

#ifndef MATRIX_FUNCTIONS_H
#define MATRIX_FUNCTIONS_H

#include "Matrix.h"

class slae // 'slae' stands for System of Linear Algebraic Equations
{
public:
#ifdef TESTING
friend class slaeTest;
#endif
	class Solution;
public:
	static slae::Solution Gauss(const Matrix &A, const Matrix &b);
	/**
	 * Solves system of linear algebraic equations (SLAE) made from extended matrix (A|b).
	 * A - coefficients at variables, b - column vector of constant terms
	 * Returns object of class 'slae::Solution'.
	 * If SLAE cannot be made from A and b - throw invalid_argument
	 */
private:
	static Matrix extend(const Matrix &A, const Matrix &b);
	/// Creates extended matrix of A and b.

	static slae::Solution solve(Matrix &AbTriangle) noexcept;
	/**
	 * Returns solution of 'AbTriangle' as object of slae::Solution.
	 * 'AbTriangle' is extended matrix in upper-triangle form.
	 */

	static void setZeroUpper(Matrix &m, size_t leadRow, size_t leadColumn);
	/**
	 * Sets all elements in column below lead element as 0 by elementary transformation of rows of matrix.
	 * Throws out_of_range if 'leadRows' or 'leadColumn' are inappropriate.
	 */
};

class slae::Solution
		/**
		 * Class for solution of SLAE.
		 *
		 * Three bool fields say if SLAE has only one solution,
		 * 	doesn't have a solution or has infinitely many solutions.
		 * One of them is necessarily true, while two other are false.
		 *
		 * 'solution' is matrix with solution of SLAE.
		 * If SLAE has only one solution 'solution' is vector.
		 * If SLAE doesn't have solutions or has infinitely many solutions -
		 * 	'solution' is 1x1 matrix with only element - 0.
		 */
{
public:
	Solution setOnly(const Matrix &solution_) noexcept;
	Solution setNone() noexcept;
	Solution setInfinitely() noexcept;

	[[nodiscard]] bool is_valid() const noexcept;
	[[nodiscard]] Matrix getSolution() const;

	bool operator==(const Solution &other) const noexcept;
private:
	Matrix solution = Matrix();
	bool only = false;
	bool none = true;
	bool infinitely = false;
};

#ifdef TESTING
class slaeTest
{
public:
	static int test_extend();
	static int test_solve();
};
#endif



#endif //MATRIX_FUNCTIONS_H
