#include "Matrix.h"
#include "functions.h"
#include <iostream>
#include <string>

using namespace std;

Matrix HilbertsMatrix(size_t rows, size_t columns)
{
	Matrix res(rows, columns);
	for (size_t i = 0; i < res.rows(); ++i)
	{
		for (size_t j = 0; j < res.columns(); ++j)
			res[i][j] = 1. / (i + j + 1);
	}
	return res;
}

void testGauss(const Matrix &matrix)
{
	cout << "********************" << endl;
	cout << "rank = " << matrix.rank() << endl;
	cout << "det = " << matrix.det() << endl;
	Matrix matrix_b(matrix.rows(), 1), matrix_res(matrix.rows(), 1);
	matrix_res.fillRandom();
	cout << "Matrix =\n " << matrix << endl;
	matrix_b = matrix * matrix_res;
	cout << "b =\n" << matrix_b << endl;
	cout << "res =\n" << matrix_res << endl;
	auto solution = slae::Gauss(matrix, matrix_b);
	if (solution.is_valid())
	{
		cout << solution.getSolution();
		cout << "Predictable solution =\n" << matrix_res;
	}
	else
		cout << "This matrix can't be solved" << endl;
	cout << "********************" << endl;
}

int main()
{
	try
	{
		testGauss(Matrix(3, 3).fillRandom());
		testGauss(HilbertsMatrix(3, 3));
		testGauss(Matrix(10, 10).fillRandom());
		testGauss(HilbertsMatrix(10, 10));
		testGauss(Matrix(50, 50).fillRandom());
		testGauss(HilbertsMatrix(50, 50));
	}
	catch (const exception &ex)
	{
		cout << ex.what() << endl;
	}
	return 0;
}
