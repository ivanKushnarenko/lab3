#include "Matrix.h"
#include "functions.h"
#include "../../googletest/googletest/include/gtest/gtest.h"


TEST(ClassTest, RankTest)
{
	Matrix m1(3,3);
	m1 = {1, -1, 2,
	 	  2, -2, 4,
	 	  -1, 1, -2};
	EXPECT_EQ(1, rank(m1));
	Matrix m2(2, 3);
	m2 = {1, -1, 2,
	      0, 1, -1};
	EXPECT_EQ(2, rank(m2));
	Matrix m3(2, 2);
	m3 = {0, 0, 0, 0};
	EXPECT_EQ(0, rank(m3));
	Matrix m4;
	EXPECT_EQ(0, rank(m4));
	m4.at(0, 0) = 1;
	EXPECT_EQ(1, rank(m4));
	Matrix m5(3, 5);
	m5 = {-4, 3, 9, 5, -1,
	      7, -1, 8, 15, 6,
	      5, 2, 1, 6, 7};
	EXPECT_EQ(3, rank(m5));
}

TEST(ClassTest, DetTest)
{
	Matrix m1(3, 3);
	m1 = {1, -1, 2,
		  2, -2, 4,
		  -1, 1, -2};
	EXPECT_EQ(0, det(m1));
	Matrix m2;
	EXPECT_EQ(0, det(m2));
	m2.at(0, 0) = 5768;
	EXPECT_EQ(5768, det(m2));
	Matrix m3(3, 3);
	m3 = {1, -2, 3,
	      4, 0, 6,
	      -7, 8, 9};
	EXPECT_EQ(204, det(m3));
}

TEST(SLAE_Test, GaussTest)
{
	try
	{
		Matrix m1(3, 3), m1_b(3, 1), m1_answer(3, 1);
		m1 = {3, 2, -5,
		      2, -1, 3,
		      1, 2, -1};
		m1_b = {-1,
		  		13,
		  		9};
		m1_answer = {3,
			   		 5,
			   		 4};
		slae::Solution m1_solution = slae::Gauss(m1, m1_b);
		if (m1_solution.is_valid())
		{
			std::cout << "#m1_solution is valid" << std::endl;
			Matrix m1_res = m1_solution.getSolution();
			EXPECT_EQ(m1_answer, m1_res);
		}
		else
		{
			std::cout << "#m1_solution is not valid" << std::endl;
		}


		Matrix m2(3, 4), m2_b(3, 1);
		m2 = {4, -3, 2, -1,
		 	  3, -2, 1, -3,
		 	  5, -3, 1, -8};
		m2_b = {8, 7, 1};

		slae::Solution m2_solution = slae::Gauss(m2, m2_b);
		if (m2_solution.is_valid())
		{
			std::cout << "#m2_solution is valid" << std::endl;
		}
		else
		{
			std::cout << "#m2_solution is not valid" << std::endl;
		}

	}
	catch (const std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
}


int main()
{
	try
	{
		::testing::InitGoogleTest();
		return RUN_ALL_TESTS();
	}
	catch (const std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}
}
